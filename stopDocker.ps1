﻿# Arrêt de Docker

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"


Write-Output "" >> $pathlog\ecombox.log
Write-Output "===========================================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Le script stopDocker a été lancé" >> $pathlog\ecombox.log
Write-Output "===========================================================================" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log


$process = Get-Process "com.docker.backend" -ErrorAction SilentlyContinue
if ($process.Count -gt 0)
{
    Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend existe et va être stoppé" >> $pathlog\ecombox.log
    Stop-Process -Name "com.docker.backend" -Force  >> $pathlog\ecombox.log
}
    else {
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend n'existe pas" >> $pathlog\ecombox.log
     }


$service = get-service com.docker.service -ErrorAction SilentlyContinue
if ($service.status -eq "Stopped")
{

    Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Docker est déja arrêté." >> $pathlog\ecombox.log
    Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Docker est déjà arrêté."
}
    else
    {
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le service Docker va être stoppé." >> $pathlog\ecombox.log
      Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le service Docker va être stoppé."
      net stop com.docker.service >> $pathlog\ecombox.log
    }


foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Running"}))
{
    $svc | Stop-Service -ErrorAction Continue -Confirm:$false -Force
    $svc.WaitForStatus('Stopped','00:00:20')
}

Get-Process | Where-Object {$_.Name -ilike "*docker*"} | Stop-Process -ErrorAction Continue -Confirm:$false -Force

Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Docker est arrêté." >> $pathlog\ecombox.log
Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Docker est arrêté."
