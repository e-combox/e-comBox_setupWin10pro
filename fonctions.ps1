﻿# Déclaration des variables

$branche='v3'
$version_setup='3.0'
$version_appli='3.0'
$tag='3.0'
$depot_gitlab_setup='https://gitlab.com/e-combox/e-comBox_setupWin10pro'
$depot_gitlab_portainer='https://gitlab.com/e-combox/e-comBox_portainer'
$depot_gitlab_rv='https://gitlab.com/e-combox/e-comBox_reverseproxy'

$path_portainer="$env:USERPROFILE\e-comBox_portainer"
$path_rv="$env:USERPROFILE\e-comBox_reverseproxy"
$path_docker="\\wsl$\docker-desktop-data\version-pack-data\community\docker"
$path_migration="$env:USERPROFILE\.docker\migration"

# Déclaration de la fonction pour installer les correctifs

. "$pathscripts\fonction_verifPatch.ps1"
. "$pathscripts\fonction_patch.ps1"


Function valeurConfig
{
    $path = "$env:USERPROFILE\.docker\confEcombox\param.conf"
    If (-not (Test-Path $path))
    {
     curl $depot_gitlab_setup/raw/$branche/param.conf -OutFile $path
    }
    
    $file = new-Object System.IO.StreamReader(Get-item –Path "$path")
    $array = @{}    
    while($line = $file.ReadLine())
    {
        $splitLine = $line.Split('=')
        $id = $splitLine[0]
        $value = $splitLine[1]
        $array.Add($id,$value)
    }    
    $file.Close()
    return $array    
} 


Function ajoutInfosLog
{
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le système utilisé" >> $pathlog\ecombox.log
    Get-ComputerInfo | select OSName, OsArchitecture, OsVersion, WindowsVersion, WindowsCurrentVersion, WindowsBuildLabEx, OsHardwareAbstractionLayer, OsTotalVisibleMemorySize, OsFreePhysicalMemory, OsTotalVirtualMemorySize, OsFreeVirtualMemory, OsInUseVirtualMemory, OsTotalSwapSpaceSize | fl  >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le CPU et la RAM utilisé par Docker" >> $pathlog\ecombox.log
    docker info 2>$null| Select-String "CPU" >> $pathlog\ecombox.log
    docker info 2>$null | Select-String "Memory" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le proxy configuré sur Docker" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Configuration de settings.json" >> $pathlog\ecombox.log
    Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | Select-String "proxyHttp" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Configuration de http_proxy.json" >> $pathlog\ecombox.log
    Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\http_proxy.json" >> $pathlog\ecombox.log
}


Function recupIPhost
{
    # Récupération et mise au bon format de l'adresse IP de l'hôte (l'adresse IP récupérée est celle associée à une passerelle par défaut)
    #$docker_ip_host = (Get-NetIPAddress -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPv4Address
    $docker_ip_host = (Get-NetIPAddress -InterfaceIndex (Get-NetIPConfiguration | Foreach IPv4DefaultGateway).ifIndex).IPv4Address  | Select-Object -first 1
    $docker_ip_host = "$docker_ip_host"
    $docker_ip_host = $docker_ip_host.Trim()

    return $docker_ip_host
}


Function recupIPvalides
{
   # Récupération des adresses IP d'une interface physique même si elles ne sont pas associées à une passerelle par défaut
   #$adressesIPvalides = (Get-NetIPAddress -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPv4Address | Select-String -NotMatch ^169
   $adressesIPValides = (Get-NetIPAddress -AddressFamily IPv4 -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPAddress | Select-String -NotMatch ^169
        
   $adressesIPvalides = "$adressesIPvalides"
   $adressesIPvalides = $adressesIPvalides.Trim()

   return $adressesIPvalides
  
}


Function verifIPvalide {
    $config = valeurConfig
    $docker_ip_host = $config.ADRESSE_IP

    # Récupération des adresses IP d'une interface physique même si elles ne sont pas associées à une passerelle par défaut
    $adressesIPvalides = recupIPvalides

    if ($docker_ip_host -notlike $adressesIPvalides) {
        Write-Output "`nAdresse IP non valide"
        $stop = popupExclamation -titre "Adresse IP non valide" -message "Le système détecte l'adresse IP valide utilisé dans l'e-comBox n'est pas valide.`n`nVeuillez procéder à la réinitialisation de l'environnement et relancer l'application." 
        # Ce n'est pas la peine de continuer
        Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
        Write-host ""
}
}


Function popupInformation 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’)
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 15, $titre, 64) 
   return "$codeBouton"
}


Function popupExclamation 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 48) 
}


Function popupQuestion 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 36) 
}


Function popupQuestionDefautNon 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 292) 
}


Function popupStop 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 20) 
}


Function testConnectInternet
{
   $adressesIPvalides = recupIPvalides 

   if (($adressesIPvalides -eq $null) -or ! (Test-Connection gitlab.com -ErrorAction SilentlyContinue)) {
 
      $valideIP = popupExclamation -titre "Configuration de l'adresse IP" -message "Le système ne détecte aucune adresse IP permettant l'accès à Internet nécessaire pour pouvoir utiliser e-comBox. `n`nPour autant l'adresse IP $docker_ip_host figure dans le fichier de paramètres mais l'application ne peut être configuré avec celle-ci. `n`nVérifiez votre configuration IP et relancez le programme." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log  
      Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
      Write-host ""
      exit
    }
}


Function demarreDocker
{

    $WarningPreference='silentlycontinue'

    #Lancement de Docker en super admin
    #if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -WindowStyle Normal -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }
         Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Arrêt des processus résiduels." >> $pathlog\ecombox.log         

         $process = Get-Process "com.docker.backend" -ErrorAction SilentlyContinue
         if ($process.Count -gt 0)
         {
            Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend existe et va être stoppé" >> $pathlog\ecombox.log
            Stop-Process -Name "com.docker.backend" -Force  >> $pathlog\ecombox.log        
         }
            else {
                Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend n'existe pas" >> $pathlog\ecombox.log                                 
            }


         $service = get-service com.docker.service
         if ($service.status -eq "Stopped")
         {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Rien à faire car Docker est arrêté." >> $pathlog\ecombox.log             
         }
            else
            {
               Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Le service Docker va être stoppé." >> $pathlog\ecombox.log
               net stop com.docker.service >> $pathlog\ecombox.log              
            }


           foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Running"}))
           {
              $svc | Stop-Service -ErrorAction Continue -Confirm:$false -Force
              $svc.WaitForStatus('Stopped','00:00:20')               
           }

           Get-Process | Where-Object {$_.Name -ilike "*docker*"} | Stop-Process -ErrorAction Continue -Confirm:$false -Force            

          foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Stopped"} ))
          {
             $svc | Start-Service 
             $svc.WaitForStatus('Running','00:00:20')              
          }

          Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Démarrage de Docker"
          & "C:\Program Files\Docker\Docker\Docker Desktop.exe"
          $startTimeout = [DateTime]::Now.AddSeconds(90)
          $timeoutHit = $true
           
          while ((Get-Date) -le $startTimeout)
          {

          Start-Sleep -Seconds 10                     

          try
         {
            $info = (docker info 2>$null)
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `tDocker info executed. Is Error?: $($info -ilike "*error*"). Result was: $info" >> $pathlog\ecombox.log            
            if ($info -ilike "*error*")
            {
               Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `tDocker info had an error. throwing..." >> $pathlog\ecombox.log
               throw "Error running info command $info"                
            }
            $timeoutHit = $false
            break
         }
         catch 
         {

             if (($_ -ilike "*error during connect*") -or ($_ -ilike "*errors pretty printing info*")  -or ($_ -ilike "*Error running info command*"))
             {
                 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) -`t Docker Desktop n'a pas encore complètement démarré, il faut attendre." >> $pathlog\ecombox.log
                 Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) -`t Docker Desktop n'a pas encore complètement démarré, il faut attendre."
             }
             else
            {
                write-host ""
                Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer."
                #write-host "Unexpected Error: `n $_" 2>$null
                #Write-Output "Unexpected Error: `n $_" 2>$null >> $pathlog\ecombox.log
                return
            }
         }
         $ErrorActionPreference = 'Stop'
     }
     if ($timeoutHit -eq $true)
     {
         throw "Délai d'attente en attente du démarrage de docker"
     }
        
     Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer."
     Write-host ""
     Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer." >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log 
}


Function verifDocker
{
    # Vérification que Docker fonctionne correctement sinon ce n'est pas la peine de continuer

    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification de Docker." >> $pathlog\ecombox.log
    Write-host ""
    Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification de l'état de Docker"
    Write-host ""

    docker-compose version *>> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    docker info 2>$null *>> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log

    $error.clear()
    $info_docker = (docker info 2>$null)

    if (($info_docker -ilike "*error*") -or ($error -ilike "*error*") -or ($info_docker -ilike "*pas reconnu comme nom*"))
      {      
         Write-Output "Docker n'est pas démarré. Le processus doit démarrer Docker avant de continuer..." >> $pathlog\ecombox.log 
         Write-Output "" >> $pathlog\ecombox.log
         Write-host "Le processus doit démarrer Docker avant de continuer..."
         write-host ""
         demarreDocker    
      }
      else {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Docker est démarré. Le processus peut continuer..." >> $pathlog\ecombox.log 
            Write-Output "" >> $pathlog\ecombox.log
            Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Docker est démarré. Le processus peut continuer..." 
            Write-Host ""          
            }

}


Function verifPortainerApresVerifDocker
{
    # Vérification que Portainer fonctionne correctement sinon on redémarre Docker
    if (Test-Path "$path_portainer") {

    if ((docker ps -a -f "status=created" | Select-String e-combox) -or (docker ps -a -f "status=created" | Select-String portainer) -or (docker ps -a -f "status=exited" | Select-String e-combox) -or (docker ps -a -f "status=exited" | Select-String portainer)) 
       { 
         Write-Output "Portainer a mal redémarré. Le processus doit re-démarrer Docker avant de continuer..." >> $pathlog\ecombox.log 
         Write-Output "" >> $pathlog\ecombox.log
         Write-host "Le processus doit re-démarrer Docker avant de continuer..."
         write-host ""
         demarreDocker    
       }
       else {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Portainer est correctement lancé. Le processus peut continuer..." >> $pathlog\ecombox.log 
            Write-Output "" >> $pathlog\ecombox.log
            #Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Portainer est correctement lancé. Le processus peut continuer..." 
            #Write-Host ""          
            }
     }
      else {
           Write-Output "Dossier Portainer inexistant - Portainer n'est pas encore installé, ceci est normal en phase d'initialisation." >> $pathlog\ecombox.log 
           Write-Output "" >> $pathlog\ecombox.log
           #Write-host "Dossier Portainer inexistant - Portainer n'est pas encore installé, ceci est normal en phase d'initialisation"
           #write-host ""
     }
}


Function wslConfig
{
    # Récupération de la mémoire max du PC
    $memoire_pc = (Get-ComputerInfo).CsPhyicallyInstalledMemory/1MB

    # Récupération de la mémoire max utilisée par Docker/WSL2 via le fichier .wslconfig
    $ligne_mem = Get-Content $env:USERPROFILE\.wslconfig | Select-String memory
    $memoire_wsl2 = ("$ligne_mem".Split('='))[1]
    
    # Configuration éventuelle de la mémoire vive utilisée par WSL2
    $reponse = popupQuestionDefautNon -titre "Configuration de la mémoire vive (RAM) utilisée" -message "Vous disposez au total de $memoire_pc Gigabit et l'e-comBox utilise $memoire_wsl2.`n`nVous pouvez changer ce paramètre sachant que Windows a besoin d'au moins 4GB de libre pour fonctionner correctement.`n`nDésirez-vous modifier la quantité de RAM dédiée à e-comBox ?" -Foreground Yellow 
 
    if ($reponse -eq 6) 
     {
        $RAM=Read-Host "`n`nSaisissez la quantité de mémoire vive (RAM) que vous désirez allouer à l'e-comBox en saisissant également l'unité (par exemple 8GB ou 512MB)."
             
        # Affectation de cette mémoire au fichier .wslconfig
        (Get-Content -Path $env:USERPROFILE\.wslconfig) | ForEach-Object {$_ -replace "$ligne_mem", "memory=$RAM"} | Set-Content -Path $env:USERPROFILE\.wslconfig
                
        # Redémarrage de wsl
        $info = popupInformation -titre "Redémarrage de Docker" -message "Docker va automatiquement redémarrer pour prendre en compte les modifications."
        wsl --shutdown
        demarreDocker
     }
}


Function retourneAdresseProxy {

    $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"

    $settings = Get-ItemProperty -Path $reg

    if ($settings.ProxyEnable -eq 1) {
       $adresseProxy = $settings.ProxyServer
       if ($adresseProxy -ilike "*=*")
           {
            $adresseProxy = $adresseProxy -replace "=","://" -split(';') | Select-Object -First 1
            $adresseProxy = $adresseProxy.TrimStart("http://")
           }

           else
          {
            #$adresseProxy = "http://" + $adresseProxy
            $adresseProxy = $adresseProxy
        }
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "le proxy est activé et configuré à $adresseProxy" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
       
    }
    else {
       $adresseProxy = ""
    }

   # Mise à jour du fichier de paramètres sans le HTTP
   $ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_PROXY
   (Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_PROXY=$adresseProxy"} | Set-Content -Path $pathconf\param.conf
  
   return $adresseProxy
  
}


Function retourneNoProxy {

  $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"

  $settings = Get-ItemProperty -Path $reg

  if ($settings.ProxyEnable -eq 1) {
    $noProxy = $settings.ProxyOverride

    if ($noProxy)
       { 
             $noProxy = $noProxy.Replace(';',',')
       }
       else
       {     
             $noProxy = "localhost"
       }

    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "le proxy est activé et le noProxy est $noProxy" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
  }
   else {
    $noProxy = ""
  }

   # Mise à jour du fichier de paramètres
   $ligne = Get-Content $pathconf\param.conf | Select-String NO_PROXY
   (Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "NO_PROXY=$noProxy"} | Set-Content -Path $pathconf\param.conf
  
  return $noProxy
}


Function verifNoProxy {
    
    $config = valeurConfig
    $noProxy = $config.NO_PROXY
    $adresse_ip = $config.ADRESSE_IP    
    $cidr = (Get-NetIPAddress -IPAddress $adresse_ip).PrefixLength

    Write-Output "La configuration IP du poste est $adresse_ip/$cidr" >> $pathlog\ecombox.log
   
    # Conversion du masque cidr en binaire
    [Int[]]$array = (1..32) 
    for($i=0;$i -lt $array.length;$i++){ 
       if($array[$i] -gt $cidr){$array[$i]="0"} else{$array[$i]="1"} 
    } 
      $cidr_binaire =$array -join ""       
      
    # Conversion du masque binaire en décimal    
    $mask=([System.Net.IPAddress]"$([System.Convert]::ToInt64($cidr_binaire,2))").IPAddressToString
   

    # Détermination de l'adresse réseau
    $ip = [ipaddress]$adresse_ip
    $mask = [ipaddress]$mask
    $reseau = ([ipaddress]($ip.Address -band $mask.Address)).IPAddressToString 2>>$null
    
    Write-Output "Le réseau est $reseau" >> $pathlog\ecombox.log
    
    # Ajout du caractère générique "*"
    if ($cidr -le 8) {
        $reseauNoProxy = $reseau.TrimEnd("0")
        $reseauNoProxy = $reseauNoProxy.TrimEnd(".")
        $reseauNoProxy = $reseauNoProxy.TrimEnd("0")
        $reseauNoProxy = $reseauNoProxy.TrimEnd(".")
        $reseauNoProxy = $reseauNoProxy.TrimEnd("0")        
    } 
        else {
            if ($cidr -ge 24) {
               $reseauNoProxy = $reseau.TrimEnd("0")          
            }
            else {
                $reseauNoProxy = $reseau.TrimEnd("0")
                $reseauNoProxy = $reseauNoProxy.TrimEnd(".")
                $reseauNoProxy = $reseauNoProxy.TrimEnd("0")                
            }
        }

    if ($cidr -le 24) {
        $reseauNoProxy = $reseauNoProxy+"*"        
    } 

    Write-Output "Le réseau à ajouter au noProxy est $reseauNoProxy" >> $pathlog\ecombox.log
    return $reseauNoProxy       
}


Function configProxyGit 
{ 

 $config = valeurConfig
 $adresseProxy = $config.ADRESSE_PROXY
 $noProxy = $config.NO_PROXY

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Configuration du Proxy pour GIT" >> $pathlog\ecombox.log 
 Write-Output "" >> $pathlog\ecombox.log


 Set-Location -Path $env:USERPROFILE

 if ($adresseProxy) {      
    # Configuration de Git
    $adresseProxy = "http://" + $config.ADRESSE_PROXY
    git config --global http.proxy $adresseProxy *>> $pathlog\ecombox.log
    }

    else {
      # Configuration de Git
      git config --global --unset http.proxy *>> $pathlog\ecombox.log
      }
    
}


Function configProxyDocker
{
 $adProxy=retourneAdresseProxy
 $byPass=retourneNoProxy

 $config = valeurConfig
 $adresseProxy = $config.ADRESSE_PROXY
 $noProxy = $config.NO_PROXY

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Configuration du proxy pour Docker" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 
 Set-Location -Path $env:USERPROFILE\.docker

 if ($adresseProxy) {

     new-item "config.json" –type file -force *>> $pathlog\ecombox.log     

     If ($? -eq 0) {
        $allProcesses = Get-Process
        foreach ($process in $allProcesses) { 
             $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\.docker\config.json"} | Stop-Process -Force -ErrorAction SilentlyContinue
        }
        Remove-Item "config.json" *>> $pathlog\ecombox.log
        New-Item -Path "config.json" -ItemType file -force *>> $pathlog\ecombox.log
       }

$adresseProxy = "http://" + $config.ADRESSE_PROXY

@"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "$adresseProxy",
     "httpsProxy": "$adresseProxy",
     "noProxy": "$noProxy"
   }
 }
}
"@ > config.json

     Set-Content config.json -Encoding ASCII -Value (Get-Content config.json) *>> $pathlog\ecombox.log

     Write-Output ""  >> $pathlog\ecombox.log
     Write-Output "Le fichier config.json a été créé et complété."  >> $pathlog\ecombox.log
     Write-Output ""  >> $pathlog\ecombox.log
     }
     else {
         remove-item "config.json" *>> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log
         Write-Output "Le proxy est désactivé et le fichier config.json a été supprimé"  >> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log         
      }
}


Function configProxyDockerAvecPopup
{

 $adProxy=retourneAdresseProxy
 $byPass=retourneNoProxy

 $config = valeurConfig
 $adresseProxy = $config.ADRESSE_PROXY
 $noProxy = $config.NO_PROXY 

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "Popup pour la vérification de la configuration du Proxy sur Docker" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 Set-Location -Path $env:USERPROFILE\.docker

 if ($adresseProxy) {
     $reseauNoProxy = verifNoProxy
     popupExclamation -titre "Configuration du proxy" -message "Le système a détecté que vous utilisez un proxy pour vous connecter à Internet, vérifiez que ce dernier soit correctement configuré au niveau de Docker avec les paramètres suivants :`n`nAdresse IP du proxy (avec le port utilisé) pour HTTP et HTTPS : $adresseProxy `nBy Pass : $noProxy `n`n`Vérifiez que les valeurs $reseauNoProxy et localhost ou 127.0.0.1 figurent bien dans les valeurs à exclure du proxy au niveau du champs By Pass. Si ce n'est pas le cas, il est nécessaire de les ajouter sur votre système et sur Docker.`n`nSi vous venez de procéder à la configuration, il faut attendre que Docker ait redémarré avant de cliquer sur OK pour continuer."
     
     new-item "config.json" –type file -force *>> $pathlog\ecombox.log

     If ($? -eq 0) {
        $allProcesses = Get-Process
        foreach ($process in $allProcesses) { 
             $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\.docker\config.json"} | Stop-Process -Force -ErrorAction SilentlyContinue
        }
        Remove-Item "config.json" *>> $pathlog\ecombox.log
        New-Item -Path "config.json" -ItemType file -force *>> $pathlog\ecombox.log
       }
$adresseProxy = "http://" + $config.ADRESSE_PROXY
@"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "$adresseProxy",
     "httpsProxy": "$adresseProxy",
     "noProxy": "$noProxy"
   }
 }
}
"@ > config.json

     Set-Content config.json -Encoding ASCII -Value (Get-Content config.json) *>> $pathlog\ecombox.log

     Write-Output ""  >> $pathlog\ecombox.log
     Write-Output "Le fichier config.json a été créé et complété."  >> $pathlog\ecombox.log
     Write-Output ""  >> $pathlog\ecombox.log
     # Fin ajout

}
     else {
         #Ajout
         remove-item "config.json" *>> $pathlog\ecombox.log
         # Fin ajout
         Write-Output ""  >> $pathlog\ecombox.log
         Write-Output "Le proxy est désactivé et le fichier config.json a été supprimé"  >> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log
         
         popupExclamation -titre "Configuration du proxy" -message "Le système a détecté que vous n'utilisez pas de proxy pour vous connecter à Internet, vérifiez que cette fonctionnalité soit bien désactivée sur Docker. `n`nSi vous venez de procéder à la désactivation, il faut attendre que Docker ait redémarré avant de cliquer sur OK pour continuer."    
      }

}


function verifCoherenceProxy {

    $http_mode=Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | Select-String "proxyHttpMode"
    $http_mode=$http_mode.Line.Split(":")[1].TrimEnd(",").trim()


    # Proxy désactivé sur Docker Desktop mais encore pris en compte par Docker (bug de Docker)

    $http_proxy=docker info 2>$null | Select-String "HTTP Proxy"
        
    if ($http_mode -eq "false" -and ($http_proxy -isnot $null -or $http_proxy -ne "")) {
   
       write-Output "Le Proxy est désactivé sur Docker Desktop mais Docker continue à le prendre en compte."
       $stop = popupExclamation -titre "Modification du proxy" -message "Le système constate que le proxy est désactivé sur Docker Desktop mais qu'il est encore pris en compte par l'application.`n`nLa modification nécessaire va être réalisée et Docker sera redémarré." 

       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
       $content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"overrideProxyHttp`":.+","`"overrideProxyHttp`": `"`"," } 
       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
       $content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"overrideProxyHttps`":.+","`"overrideProxyHttps`": `"`"," } 
       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
       $content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"overrideProxyExclude`":.+","`"overrideProxyExclude`": `"`"," } 
       Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
   
       demarreDocker

       write-Output "Le Proxy a été correctement désactivé."

     }

    #vérif si le proxy doit être activé ou désactivé sur Docker Desktop
    $config = valeurConfig
    $docker_ip_proxy = $config.ADRESSE_PROXY

    # Proxy dans param.conf non configuré sur Docker Desktop
    if ($http_mode -eq "false" -and ($docker_ip_proxy -isnot $null -or $docker_ip_proxy -ne "")) {
       Write-Output "`nIl y a un proxy sur le système qui n'est pas configuré sur Docker Desktop."
       $stop = popupExclamation -titre "Proxy à configurer sur Docker Desktop" -message "Le système détecte un proxy sur le système qui n'est pas configuré sur Docker Desktop.`n`nVeuillez procéder à la réinitialisation de l'environnement et relancer l'application." 
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
    }

    # Proxy configuré sur Docker Desktop non configuré dans param.conf
    if (($http_mode -eq "true") -and ($docker_ip_proxy -is $null -or $docker_ip_proxy -eq "")) {
        Write-Output "`nIl y a un proxy configuré sur Docker Desktop non configuré sur l'application."
        $stop = popupExclamation -titre "Proxy configuré sur Docker Desktop" -message "Le système détecte que le proxy est activé sur Docker Desktop mais n'est pas pris en compte par l'application.`n`nVeuillez procéder à la réinitialisation de l'environnement ou désactiver le proxy sur Docker Desktop puis  relancer l'application." 
        # Ce n'est pas la peine de continuer
        Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
        Write-host ""
    }

    # Proxy système non configuré dans param.conf
    if ($docker_ip_proxy -eq "" -or $docker_ip_proxy -is $null) {
        $adresse_proxy = retourneAdresseProxy
        if ($adresse_proxy -ne "") {
            Write-Output "`nIl y a un proxy sur le système qui n'est pas configuré pour e-comBox"
            $stop = popupExclamation -titre "Proxy à configurer sur e-comBox" -message "Le système détecte un proxy sur le système qui n'est pas configuré pour e-comBox.`n`nVeuillez procéder à la réinitialisation de l'environnement et relancer l'application." 
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
        }
    }

}


Function recupPortainer 
{ 
Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Récupération de portainer" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

$TestPath=Test-Path $path_portainer
$git_portainer=-join($depot_gitlab_portainer, ".git")

If ($TestPath -eq $False) {
    # Récupération de Portainer sur Git
    Write-Output ""  >> $pathlog\ecombox.log
    Write-Output "Téléchargement de Portainer" >> $pathlog\ecombox.log
    write-host ""
    write-host "Téléchargement de Portainer"
    Set-Location -Path $env:USERPROFILE
    git clone -b $branche $git_portainer *>> $pathlog\ecombox.log
    }
    else {
      # Suppression de portainer et installation d'un éventuel nouveau Portainer
      Write-Output ""  >> $pathlog\ecombox.log
      Write-Output "    --> Portainer est installé, il faut le supprimer pour le réinstaller."  >> $pathlog\ecombox.log
      Write-Output ""  >> $pathlog\ecombox.log
      write-host ""
      write-host "Téléchargement de Portainer"
      Set-Location -Path $Path_portainer    
      docker-compose down *>> $pathlog\ecombox.log

      Set-Location -Path $env:USERPROFILE   
      Remove-Item "$path_portainer" -Recurse -Force *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "Téléchargement de Portainer" >> $pathlog\ecombox.log
      git clone -b $branche $git_portainer *>> $pathlog\ecombox.log 
    }
   
If (Test-Path "$path_portainer") {
    write-host ""
    write-host "Succès... Portainer a été téléchargé."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Succès... Portainer a été téléchargé." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log

}
    else {
       write-host ""
       write-host "Portainer n'a pas pu être téléchargé. Il y a un problème avec la commande git. Le processus va s'arrêter car ce n'est pas la peine de continuer. Consultez les logs pour avoir les détails de l'erreur."
       Write-Host ""
       write-Output "" >> $pathlog\ecombox.log
       write-Output "Portainer n'a pas pu être téléchargé. Il y a un problème avec la commande git" >> $pathlog\ecombox.log
       write-Output "" >> $pathlog\ecombox.log
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
       exit
    }      
}


Function configPortainer 
{
$config = valeurConfig
Set-Location -Path $path_portainer *>> $pathlog\ecombox.log

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de Portainer" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de Portainer" 

# Récupération de l'IP du poste
$docker_ip_host = $config.ADRESSE_IP

# Récupération du port (8880 par défaut) dans le fichier paramètre
$portainer_port = $config.PORT_PORTAINER

# Mise à jour de l'adresse IP et du port pour Portainer dans le fichier ".env"
New-Item -Name ".env" -ItemType file -force *>> $pathlog\ecombox.log

If ($? -eq 0) {
  $allProcesses = Get-Process
  foreach ($process in $allProcesses) { 
    $process.Modules | where {$_.FileName -eq "$path_portainer\.env"} | Stop-Process -Force -ErrorAction SilentlyContinue
  }

Remove-Item ".env"  *>> $pathlog\ecombox.log
New-Item -Path ".env" -ItemType file -force  *>> $pathlog\ecombox.log
}

Write-Output ""  >> $pathlog\ecombox.log
Write-Output "le fichier .env a été créé"  >> $pathlog\ecombox.log
Write-Output ""  >> $pathlog\ecombox.log

@"
URL_UTILE=$docker_ip_host
PORT=$portainer_port
"@ > .env

Set-Content .env -Encoding ASCII -Value (Get-Content .env)
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été mis à jour avec l'adresse IP $docker_ip_host et le port $portainer_port"  >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

}


Function configPortainerAvecPopup 
{
#$config = valeurConfig
}

Function startPortainer 
{ 
# Lancement de Portainer - URL : http://localhost:8880/portainer)
Set-Location -Path $path_portainer

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement de Portainer" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement de Portainer" 

If (docker ps -a | Select-String "name=portainer-app") {
   docker rm -f portainer-app *>> $pathlog\ecombox.log
   }
If (docker ps -a | Select-String "name=portainer-proxy") {
   docker rm -f portainer-proxy *>> $pathlog\ecombox.log
   }
If (docker ps -a --format '{{.Names}}' |  Select-String portainer-proxy) {
   docker rm -f $(docker ps -a --format '{{.Names}}' |  Select-String portainer-proxy)  *>> $pathlog\ecombox.log
   }
docker-compose up -d *>> $pathlog\ecombox.log

}


Function verifPortainer {
if ((docker ps |  Select-String portainer-proxy)) {
    write-host ""
    write-host "Portainer est UP, on peut continuer."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Portainer est UP, on peut continuer" >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log
    }
      else {
            write-host ""
            write-host "Toutes les tentatives pour démarrer Portainer ont échoué, essayez de stopper et de démarrer e-comBox puis de réinitialiser l'environnement"
            Write-Host ""
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Toutes les tentatives pour démarrer Portainer ont échoué" >> $pathlog\ecombox.log
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
            exit
            }    
}


Function recupReverseProxy 
{ 

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Récupération du reverse proxy" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

$TestPath=Test-Path $path_rv
$git_rv=-join($depot_gitlab_rv, ".git")

If ($TestPath -eq $False) {
    # Récupération du reverse proxy sur Git
    Write-Output ""  >> $pathlog\ecombox.log
    Write-Output "Téléchargement du reverse proxy" >> $pathlog\ecombox.log
    write-host ""
    write-host "Téléchargement du reverse proxy"
    Set-Location -Path $env:USERPROFILE
    git clone -b $branche $git_rv *>> $pathlog\ecombox.log
    }
    else {
      # Suppression du reverse proxy et installation d'un éventuel nouveau reverse proxy
      Write-Output ""  >> $pathlog\ecombox.log
      Write-Output "    --> Le reverse proxy existe et va être remplacé."  >> $pathlog\ecombox.log
      Write-Output ""  >> $pathlog\ecombox.log
      write-host ""

      Set-Location -Path $path_rv *>> $pathlog\ecombox.log 
      docker-compose down *>> $pathlog\ecombox.log
      docker image rm -f reseaucerta/docker-gen:2.1 *>> $pathlog\ecombox.log
      docker volume rm e-combox_reverseproxy_nginx-html *>> $pathlog\ecombox.log
      docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates *>> $pathlog\ecombox.log

      Set-Location -Path $env:USERPROFILE   
      Remove-Item "$path_rv" -Recurse -Force *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "Téléchargement du reverse proxy" >> $pathlog\ecombox.log
      write-host "Téléchargement du reverse proxy"  
      git clone -b $branche $git_rv *>> $pathlog\ecombox.log 
    }
   
If (Test-Path "$path_rv") {
    write-host ""
    write-host "Succès... Le reverse proxy a été téléchargé."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Succès... Le reverse proxy a été téléchargé." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log

}
    else {
       write-host ""
       write-host "Le reverse proxy n'a pas pu être téléchargé. Il y a un problème avec la commande git. Le processus va s'arrêter car ce n'est pas la peine de continuer. Consultez les logs pour avoir les détails de l'erreur."
       Write-Host ""
       write-Output "" >> $pathlog\ecombox.log
       write-Output "Le reverse proxy n'a pas pu être téléchargé. Il y a un problème avec la commande git" >> $pathlog\ecombox.log
       write-Output "" >> $pathlog\ecombox.log
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
       exit
    }  
}


Function configReverseProxy 
{
$config = valeurConfig
Set-Location -Path $path_rv *>> $pathlog\ecombox.log

# Récupération de l'IP du poste dans le fichier paramètre
$docker_ip_host = $config.ADRESSE_IP

# Récupération du port (8888 par défaut) dans le fichier paramètre
$rp_port = $config.PORT_RP

# Mise à jour de l'adresse IP et du port pour le reverse proxy dans le fichier ".env"
New-Item -Name ".env" -ItemType file -force *>> $pathlog\ecombox.log

If ($? -eq 0) {
  $allProcesses = Get-Process
  foreach ($process in $allProcesses) { 
    $process.Modules | where {$_.FileName -eq "$path_rv\.env"} | Stop-Process -Force -ErrorAction SilentlyContinue
  }

Remove-Item ".env"  *>> $pathlog\ecombox.log
New-Item -Path ".env" -ItemType file -force  *>> $pathlog\ecombox.log
}

Write-Output ""  >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été créé."  >> $pathlog\ecombox.log
Write-Output ""  >> $pathlog\ecombox.log

@"
URL_UTILE=$docker_ip_host
NGINX_PORT=$rp_port
"@ > .env

Set-Content .env -Encoding ASCII -Value (Get-Content .env)
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été mis à jour avec l'adresse IP $docker_ip_host et le port $rp_port"  >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

}


Function startReverseProxy 
{ 
# Lancement du Reverse Proxy

Set-Location -Path $path_rv *>> $pathlog\ecombox.log

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement du Reverse Proxy" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement du Reverse Proxy" 

Set-Location -Path $path_rv *>> $pathlog\ecombox.log
docker-compose up -d *>> $pathlog\ecombox.log

}


Function verifReverseProxy {
if ((docker ps |  Select-String nginx) -and (docker ps | Select-String docker-gen)) {
    write-host ""
    write-host "Le Reverse Proxy est UP, on peut continuer."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Le Reverse Proxy est UP, on peut continuer." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log
    }
      else {
            if (docker ps |  Select-String nginx) {Write-Output "Le conteneur nginx n'est pas démarré."}
            if (docker ps |  Select-String reseaucerta/docker-gen) {Write-Output "Le conteneur docker-gen n'est pas démarré."}
            write-host ""
            write-host "Toutes les tentatives pour démarrer le Reverse Proxy ont échoué, essayez de stopper et de démarrer e-comBox puis de réinitialiser l'environnement"
            Write-Host ""
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Toutes les tentatives pour démarrer le Reverse Proxy ont échoué" >> $pathlog\ecombox.log
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
            exit
            }    
}


Function startRegistry 
{ 
Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Lancement du registry" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

if ((docker ps |  Select-String e-combox_registry)) {
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Le registry existe déjà et est lancé" >> $pathlog\ecombox.log
     # On arrête et on supprime le conteneur pour pouvoir le mettre à jour avec une éventuelle nouvelle image
     # docker stop e-combox_registry      
     # docker rm -f e-combox_registry
             
   } else {   
       docker run -d -p 5000 --restart=always --name e-combox_registry -e REGISTRY_STORAGE_DELETE_ENABLED=true -e VIRTUAL_HOST=registry -v registry_data:/var/lib/registry registry:2.7 

       # Connexion du registry au réseau de l'e-combox (étape obligatoire sinon ça ne fonctionne pas si on met directement le registry dans le réseau de l'e-comBox)
       docker network disconnect bridge e-combox_registry
       docker network connect bridge_e-combox e-combox_registry
     }
}


Function startGitServer 
{ 

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Lancement de serveur Git local" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

if ((docker ps -a |  Select-String e-combox_gitserver)) {
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Le serveur Git local existe déjà et est lancé" >> $pathlog\ecombox.log
     # On arrête et on supprime le conteneur pour pouvoir le mettre à jour avec une éventuelle nouvelle image
     docker stop e-combox_gitserver
     docker rm -f e-combox_gitserver
     }      

docker pull reseaucerta/git-http-server:$tag   
docker run -d -p 80 --restart=always --name e-combox_gitserver -e VIRTUAL_HOST=git -v e-combox_git-data:/git --network=bridge_e-combox reseaucerta/git-http-server:$tag        
      
}


Function deleteApplication
{
 Write-Output "" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de l'application" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "" >> $pathlog\ecombox.log
 
   #Suppression d'une éventuelle application
   if ((docker ps -a |  Select-String reseaucerta/e-combox)) {
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Suppression d'e-combox avec son volume :" >> $pathlog\ecombox.log
     docker rm -f e-combox *>> $pathlog\ecombox.log
     docker volume rm ecombox_data *>> $pathlog\ecombox.log
     docker image rm -f reseaucerta/e-combox:$version_appli      
   }
    else {
       Write-Output "" >> $pathlog\ecombox.log
       Write-Output "Pas d'application e-combox trouvée." >> $pathlog\ecombox.log      
     } 
}


Function startApplication
{
  $config = valeurConfig
  $port_externe_ecb = $config.PORT_ECB
  $ports_ecb = -join("$port_externe_ecb",":","80")
  
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Téléchargement et lancement de l'application" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "" >> $pathlog\ecombox.log

  # Téléchargement d'e-comBox
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "Téléchargement d'e-comBox :" >> $pathlog\ecombox.log
  Write-Host ""
  Write-Host "Téléchargement d'e-comBox."
       
  docker pull reseaucerta/e-combox:$version_appli *>> $pathlog\ecombox.log

  # Lancement d'e-comBox
  Write-Output "Pré-lancement d'e-comBox :" >> $pathlog\ecombox.log
  Write-Host ""
  Write-Host "Pré-lancement d'e-comBox."
         
  docker run -dit --name e-combox -v ecombox_data:/usr/local/apache2/htdocs/ -v ecombox_config:/etc/ecombox-conf --restart always -p $ports_ecb --network bridge_e-combox reseaucerta/e-combox:$version_appli *>> $pathlog\ecombox.log
  
  Write-Host ""  
  Write-host "Téléchargement et pré-lancement d'e-comBox fait FAIT."
}


Function recupMDPportainer {
# Récupération du mot de passe de portainer (portnairAdmin par défaut)
$portainer_pass = docker exec e-combox cat htdocs/main-es2015.js | Select-String "password:" | Select-String -NotMatch "//"
$portainer_pass = ($portainer_pass -replace ",","").Split("{""}")[1].TrimStart()

return $portainer_pass
}


Function configApplication
{
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
     Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de l'application" >> $pathlog\ecombox.log
     Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log

     Write-Host ""
     Write-Host "Configuration d'e-comBox." 
     
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Configuration de l'URL d'accès à Portainer" >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log 

     # Récupération de l'URL d'accès à portainer
     $ancienneURL =  docker exec e-combox bash -c 'grep -ni \"this.basePath\" htdocs/main-es2015.js | grep http | cut -d\"/\" -f3'

     # Remplacement par la bonne URL
     $env = Get-Content $path_portainer\.env
     $ip_host = $env[0].split("=")[1]
     $port_portainer = $env[1].split("=")[1]
     
     $nouvelleURL = -join("$ip_host",":","$port_portainer")

     if ($ancienneURL -ne $nouvelleURL) {
        #Write-Host "L'ancienne URL $ancienneURL sera remplacée par la nouvelle $nouvelleURL."
        Write-Output "" >> $pathlog\ecombox.log
        Write-Output "L'ancienne URL $ancienneURL sera remplacée par la nouvelle $nouvelleURL." >> $pathlog\ecombox.log
        
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main-es2015.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main-es2015.js.map
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/ecombox-ecombox-module-es2015.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/ecombox-ecombox-module-es2015.js.map
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main-es5.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main-es5.js.map
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/ecombox-ecombox-module-es5.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/ecombox-ecombox-module-es5.js.map
        }
        else {
           #Write-Host "L'ancienne URL $ancienneURL est identique à la nouvelle $nouvelleURL."
           Write-Output "" >> $pathlog\ecombox.log
           Write-Output "L'ancienne URL $ancienneURL est identique à la nouvelle $nouvelleURL." >> $pathlog\ecombox.log
           }      
      
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Configuration d'e-comBox réalisée." >> $pathlog\ecombox.log
     Write-Host ""
     Write-Host "Configuration d'e-comBox réalisée. L'application va être lancée dans votre navigateur."
}


Function nettoyageImages
{
      Write-host ""      
      Write-host "Suppression des images si elles ne sont associées à aucun site"
      Write-host ""
      write-Output "" >> $pathlog\ecombox.log
      Write-Output "Suppression des images si elles ne sont associées à aucun site" >> $pathlog\ecombox.log
      docker rmi $(docker images -q) 2>$null *>> $pathlog\ecombox.log

      # Suppression des éventuelles images dangling
      if (docker images -q -f dangling=true) {
          docker rmi $(docker images -q -f dangling=true) 2>$null *>> $pathlog\ecombox.log
      }
}


function supProxyPortainer
{
   $path = "$env:USERPROFILE\.docker\config.json"
   If (Test-Path $path) {
      remove-item "$env:USERPROFILE\.docker\config.json" *>> $pathlog\ecombox.log
   }
}


Function creerPass
{

Param (
  [string]$type)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }
	
if ($type -eq "ecb") {
   Do {

      $Cred1 = Get-Credential -Message "Saisissez le nom d'utilisateur et le mot de passe que vous voulez utiliser pour accéder à l'interface d'e-comBox."
      $Cred2 = Get-Credential -UserName $Cred1.GetNetworkCredential().UserName -Message "Saisissez de nouveau le mot de passe."
      if ($Cred1.GetNetworkCredential().Password -cne $Cred2.GetNetworkCredential().Password) {$retour = popupInformation -titre "Mot de passe non valide" -message "Les mots de passe ne correspondent pas. Veuillez recommencer !"}      
          
   } Until (($Cred1.GetNetworkCredential().Password -ceq $Cred2.GetNetworkCredential().Password))

   $Cred = $Cred1
   }

   else {
        Do {

           $Cred1 = Get-Credential -UserName "admin" -Message "Saisissez le mot de passe que vous avez attribué au compte `"admin`" de Portainer."
           $Cred2 = Get-Credential -UserName "admin" -Message "Saisissez de nouveau le mot de passe."
           if ($Cred1.GetNetworkCredential().Password -cne $Cred2.GetNetworkCredential().Password) {$retour = popupInformation -titre "Mot de passe non valide" -message "Les mots de passe ne correspondent pas. Veuillez recommencer !"}      
        
        } Until (($Cred1.GetNetworkCredential().Password -ceq $Cred2.GetNetworkCredential().Password))

        $Cred = $Cred1
      }

if ($cred) {
   
   $name = $Cred.GetNetworkCredential().UserName
   $password = $cred.GetNetworkCredential().Password
   if ($password -eq "") {
      if ($type -eq "ecb") {
          $retour = popupExclamation -titre "Mot de passe non valide" -message "Le mot de passe est vide. L'authentification à l'e-comBox se fera sans mot de passe."      
          }
          else {
             $retour = popupExclamation -titre "Mot de passe non valide" -message "Le mot de passe est vide. La synchonisation du mot de passe de Portainer ne peut pas s'effectuer. `n`nVeuillez recommencer l'opération."      
             }
      If (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName"
          }
      If (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass"
          }
      }
      else {
        $Cred.UserName | Out-File $pathconf\$fileName
        $Cred.Password | ConvertFrom-SecureString | Out-File $pathconf\$filePass
        if ($type -eq "ecb") {
          $retour = popupInformation -titre "Authentification valide" -message "Le nom d'utilisateur '$name' et le mot de passe '$password' ont été créés. `n`Merci de vider le cache de votre navigateur afin que la nouvelle authentification à l'e-comBox soit prise en compte."      
          } else {             
                 $retour = popupInformation -titre "Synchronisation du mot de passe" -message "Le mot de passe '$password' va être synchronisé avec e-comBox."
            }     
              
        Write-Output "Le nom d'utilisateur et le mot de passe ont été créés dans les fichiers $fileName et $filePass."  >> $pathlog\ecombox.log
        Write-Output ""  >> $pathlog\ecombox.log              
        }
    }
    else {
        Write-Output "L'action a été annulée."  >> $pathlog\ecombox.log
        Write-Output ""  >> $pathlog\ecombox.log
    }
}


Function creerAuth
{
If ((Test-Path "$pathconf\authName.txt") -and (Test-Path "$pathconf\authPass.txt")) {

   $name = Get-Content $pathconf\authName.txt
   $securePass = Get-Content $pathconf\authPass.txt | ConvertTo-SecureString
   $auth = New-Object System.Management.Automation.PSCredential -ArgumentList $name,$securePass
   $pass = $auth.GetNetworkCredential().Password

   # Création du .htaccess

   $test_htaccess = docker exec e-combox bash -c 'cat htdocs/.htaccess' 2>&1
      if ($test_htaccess -eq $null)
      {
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
      }
   
   docker exec e-combox bash -c 'touch htdocs/.htaccess' *>> $pathlog\ecombox.log       
   docker exec e-combox bash -c 'echo AuthType Basic > htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthName \"E-COMBOX\" >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthBasicProvider file >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthUserFile /etc/ecombox-conf/.auth >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e Require valid-user >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   # docker exec e-combox bash -c 'cat htdocs/.htaccess'

   $test_auth = docker exec e-combox bash -c 'cat /etc/ecombox-conf/.auth'
   if ($test_auth -eq "null")
      {
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
      }
   
   docker exec e-combox htpasswd -cBb /etc/ecombox-conf/.auth $name $pass *>> $pathlog\ecombox.log
   }
}


Function supprimerAuth {

Param (
  [string]$type
)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }

# Pour l'instant pas de test sur le type car seul le type "ecb" est concerné
If ((Test-Path "$pathconf\$fileName") -or (Test-Path "$pathconf\$filePass"))  {
       if (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName" -Force
          Write-Output "Le fichier $pathconf\$fileName a été supprimé."  >> $pathlog\ecombox.log
          Write-Output ""  >> $pathlog\ecombox.log
          #write-host Le fichier $pathconf\$fileName a été supprimé.
          }
       if (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass" -Force
          Write-Output "Le fichier $pathconf\$fileName a été supprimé."  >> $pathlog\ecombox.log
          Write-Output ""  >> $pathlog\ecombox.log
          #write-host Le fichier $pathconf\$filePass a été supprimé.
          }
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
       Write-Output "Les fichiers .htaccess et .auth ont été supprimés."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
     }
}      


Function supprimerAuthAvecPopup {

Param (
  [string]$type
)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }

# Pour l'instant pas de test sur le type car seul le type "ecb" est concerné
If ((Test-Path "$pathconf\$fileName") -or (Test-Path "$pathconf\$filePass"))  {
    $retour = popupStop -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "Êtes-vous certain de vouloir supprimer l'authentification pour accéder à l'e-comBox ?"      
     
    if ($retour -eq 6) 
       {
       if (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName" -Force
          }
       if (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass" -Force
          }
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
     
       $retour = popupExclamation -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "L'accès à l'e-comBox se fait maintenant sans authentification. `n`Si vous rencontrez un problème, videz le cache de votre navigateur."
       Write-Output "Les fichiers $pathconf\$fileName et $pathconf\$filePass contenant les nom d'utilisateur et le mot de passe ont été supprimés"  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
       Write-Output "Les fichiers .htaccess et .auth ont été supprimés."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log 
       }
       else {
           Write-Output "L'utilisateur a renoncé à la suppression de l'authentification."  >> $pathlog\ecombox.log
           Write-Output ""  >> $pathlog\ecombox.log
          }
    }
    else {
       $retour = popupExclamation -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "Aucune authentification pour accéder à l'interface de l'e-comBox n'est actuellement configurée."      
       Write-Output "Aucune authentification configurée : pas de proposition de suppression."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
    }
}      


Function synchroPassPortainer
{
If ((Test-Path "$pathconf\portainerName.txt") -and (Test-Path "$pathconf\portainerPass.txt")) {

   $nom = Get-Content $pathconf\portainerName.txt
   $securePass = Get-Content $pathconf\portainerPass.txt | ConvertTo-SecureString
   $auth = New-Object System.Management.Automation.PSCredential -ArgumentList $nom,$securePass
   $pass = $auth.GetNetworkCredential().Password
   
   $ancienPass = recupMDPportainer
   #$ancienPass = docker exec e-combox cat htdocs/main-es2015.js | Select-String "password:" | Select-String -NotMatch "//"
   #$ancienPass = ($ancienPass -replace ",","").Split("{""}")[1].TrimStart()

   docker exec e-combox sed -i "s/$ancienPass/$pass/g" htdocs/main-es2015.js
   docker exec e-combox sed -i "s/$ancienPass/$pass/g" htdocs/main-es5.js
   }
}


Function supprimerSites
{

$reponse = popupStop -titre "Suppression des sites" -message "il va être procédé à la suppression de vos sites, les données ne pourront pas être récupérées. `n`nLe mot de passe de Portainer va être réinitialisé et l'éventuelle authentification à l'interface va être supprimée. `n`nLe processus peut être assez long, veuillez patienter... `n`nCliquez sur Oui pour confirmer ou sur Non pour arrêter le processus."

   if ($reponse -eq 6) 
     {
       Write-Output "Suppression de l'authentification :" >> $pathlog\ecombox.log
       Write-Host "Suppression de l'authentification"
       supprimerAuth -type "ecb"
       supprimerAuth -type "p"

       if (docker ps -q) {       
           Write-Output "Arrêt des conteneurs :" >> $pathlog\ecombox.log
           docker stop $(docker ps -q) *>> $pathlog\ecombox.log          
           Write-Output "" >> $pathlog\ecombox.log
        }
      
      #docker system prune -a --volumes -f --filter "images" *>> $pathlog\ecombox.log
           
      docker rm -fv $(docker ps -a --format '{{.Names}}' |  Select-String -Pattern e-combox, docker-gen, nginx, portainer, prestashop, woocommerce, humhub, kanboard, mautic, blog, odoo, suitecrm)  *>> $pathlog\ecombox.log
      docker volume prune -f *>> $pathlog\ecombox.log
      docker image prune -af *>> $pathlog\ecombox.log
      docker network prune -f *>> $pathlog\ecombox.log

      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de tous les objets réalisée :" >> $pathlog\ecombox.log
      Write-host ""
      Write-Host "La suppression des sites est terminée, le système va procéder à la réinitialisation de l'application e-comBox."      

      # Réinitialisation de l'appli
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Réinitialisation de l'application en cours." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-host ""
      Write-Host "Réinitialisation de l'application en cours..."
      Write-Host ""
      Start-Process "$pathscripts\lanceScriptPS_initialisationApplication.bat" -wait -NoNewWindow
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-Date) - Réinitialisation de l'application réalisée." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log       
     }   
     else {
          #return $supprimeSite = $false
          Write-Output "" >> $pathlog\ecombox.log
          Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de tous les objets annulée." >> $pathlog\ecombox.log
          Write-host ""
          Write-Host "La suppression des sites a été annulée, ce n'est pas la peine de réinitialiser l'application."  
          }              
}


Function recupTailleDisque
{
     # Récupération de la taille du disque du PC en Ko
     $taille_disque=(Get-WmiObject -Class Win32_LogicalDisk -Filter "DeviceID='C:'").Size
     #write-host $taille_disque
     return $taille_disque
}


Function recupEspaceLibreDisque
{
     # Récupération de la taille libre du disk du PC en Ko
     $espace_libre_disque=(Get-WmiObject -Class Win32_LogicalDisk -Filter "DeviceID='C:'").FreeSpace
     #Write-Host $espace_libre_disque
     return $espace_libre_disque
}


Function calculPourcentageLibreDisque
{
      # Pourcentage de libre
      $taille_disque = recupTailleDisque
      $espace_libre_disque = recupEspaceLibreDisque
      # $espace_libre_disque_pourcent=($taille_disque-$espace_libre_disque)*100/$taille_disque
      $espace_libre_disque_pourcent="{0:p2}" -f (($espace_libre_disque)/$taille_disque)
      #write-host $espace_libre_disque_pourcent
      return $espace_libre_disque_pourcent
}


Function recupTailleVHD
{
     # Récupération de la taille du disk VHD en Mo
     $file = "$env:LOCALAPPDATA\Docker\wsl\data\ext4.vhdx"

     $taille_vdisk=
@"
select vdisk file=$file
attach vdisk readonly
detail vdisk
detach vdisk
exit
"@ | diskpart | Select-String "Taille physique"

     $taille_vdisk=$taille_vdisk.Line.Split(":")[1]

     # Quand le VHD ne prend pas beaucoup d'espace la taille est en MO
     if ($taille_vdisk -match "M") {
         $taille_vdisk=($taille_vdisk -replace '[a-zA-Z]+','').trim()
         $taille_vhd=[decimal]$taille_vdisk
         
     } else {
             $taille_vdisk=(($taille_vdisk -replace '[a-zA-Z]+','').trim())
             $taille_vhd=[decimal]$taille_vdisk
             $taille_vhd=$taille_vhd*1024             
            }

     return $taille_vhd
}


Function calculTailleOccupeVHD
{
     # Récupération de la taille réellement occupé en Mo
     $taille_occupe_vhd=docker system df --format '{{.Size}}'
     # Taille des images en MB
     $taille_image=($taille_occupe_vhd.GetValue(0)).trimend("GB")
     $taille_image_MB=[decimal]$taille_image*1024
     
     # Taille des conteneurs en MB
     $taille_conteneur=($taille_occupe_vhd.GetValue(1)).trimend("MB")
     $taille_conteneur_MB=[decimal]$taille_conteneur
     
     # Taille des volumes en MB
     $taille_volume=($taille_occupe_vhd.GetValue(2)).trimend("GB")
     $taille_volume_MB=[decimal]$taille_volume*1024
     
     # Taille totale de l'espace réellement occupé sur le VHD
     $taille_occupe_vhd=$taille_image_MB+$taille_conteneur_MB+$taille_volume_MB
     
     return $taille_occupe_vhd
}


Function calculEspaceAlibererMo
{
     $taille_vhd = recupTailleVHD
     $taille_occupe_vhd = calculTailleOccupeVHD

     # Espace qui peut être libéré en Mo
     $espace_libre_vhd=$taille_vhd-$taille_occupe_vhd
        
     return $espace_libre_vhd
}


Function calculEspaceAlibererPourcent
{
     $taille_vhd = recupTailleVHD
     $taille_occupe_vhd = calculTailleOccupeVHD

     # Espace qui peut être libéré en Mo
     $espace_libre_vhd=$taille_vhd-$taille_occupe_vhd
     
     # Pourcentage d'espace non occupé
     #$espace_libre_vhd_pourcent=$espace_libre_vhd*100/$taille_vhd
     $espace_libre_vhd_pourcent="{0:p2}" -f ($espace_libre_vhd/$taille_vhd)
     
     return $espace_libre_vhd_pourcent
}


Function reductionVHD {

    $file = "$env:LOCALAPPDATA\Docker\wsl\data\ext4.vhdx"

    wsl -e sudo fstrim /
    wsl --shutdown

@"
select vdisk file=$file
attach vdisk readonly
compact vdisk
detach vdisk
exit
"@ | diskpart

    # Relance de Docker
    demarreDocker

}


Function optimisationEspace {

   Write-Host "Attention à ne pas redémarrer Docker. Le système le fera lorsque les opérations seront terminées."
  
   $taille_vhd_avant = recupTailleVHD
     
   # Suppression des images non utilisées
   #nettoyageImages

   # Optimisation du VHDX
   reductionVHD

   $taille_vhd_après = recupTailleVHD

   $espace_recupere_go = ($taille_vhd_avant - $taille_vhd_après)/1KB
   $espace_recupere_go = "{0:N2}" -f $espace_recupere_go

   Write-Output "L'espace disque récupéré est de $espace_recupere_go Go" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log

   $reduc = popupExclamation -titre "Réduction effective" -message "Vous avez récupéré $espace_recupere_go Go."

}


Function verifEspace {

# Alertes en cas d'espace disque à optimiser

# Alerte en cas d'espace disque virtuel si au moins 60 % peut être libéré

Write-Output "`n`nEspace non occupé du VHD." >> $pathlog\ecombox.log

$taille_vhd = recupTailleVHD
$taille_vhd_go = $taille_vhd/1KB
Write-Output "`nLa taille du VHD est $taille_vhd Mo soit $taille_vhd_go Go." >> $pathlog\ecombox.log

$taille_occupe_vhd = calculTailleOccupeVHD
$taille_occupe_vhd_go = $taille_occupe_vhd/1KB
Write-Output "L'espace occupé du VHD est $taille_occupe_vhd Mo soit $taille_occupe_vhd_go Go." >> $pathlog\ecombox.log

$espace_libre_vhd = $taille_vhd - $taille_occupe_vhd
$espace_libre_vhd_go = $espace_libre_vhd/1KB
$espace_libre_vhd_go = "{0:N2}" -f ($espace_libre_vhd_go)

Write-Output "L'espace non occupé du VHD est $espace_libre_vhd Mo soit $espace_libre_vhd_go Go." >> $pathlog\ecombox.log

$taux_espace_libre_vhd = $espace_libre_vhd/$taille_vhd
$taux_espace_libre_vhd_pourcent="{0:p4}" -f ($taux_espace_libre_vhd)
Write-Output "Le taux d'espace non occupé du VHD est de $taux_espace_libre_vhd_pourcent." >> $pathlog\ecombox.log

# On ne réduit pas si ce n'est pas vraiment utile
if ($taille_vhd -ge 10 -and $espace_libre_vhd_go -ge 4 -and $taux_espace_libre_vhd -ge 0.6) {

   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Optimisation de l'espace" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "L'espace disque virtuel peut être libéré de $espace_libre_vhd_pourcent" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log

   $reduc = popupExclamation -titre "Réduction de l'espace disque utilisé par les sites" -message "Vous disposez de $taux_espace_libre_vhd_pourcent d'espace non utilisé par les sites. `n`nLe système va procéder à l'optimisation de l'espace occupé par l'e-comBox avant de lancer l'application.`n`nVous ne devez pas redémarrer Docker lorsque cela vous le sera demandé. Le système procèdera automatiquement au redémarrage à la fin des opérations." 
   
   $optimisation = optimisationEspace      

}

# Alerte en cas d'espace disque plein à 80 % ou d'espace libre est inférieure à 40 Go

Write-Output "`n`nEspace libre DISQUE." >> $pathlog\ecombox.log

$taille_disque = recupTailleDisque
Write-Output "`nLa taille du disque est de $taille_disque Ko." >> $pathlog\ecombox.log 

$taille_libre_disque = recupEspaceLibreDisque
$taille_libre_disque_go =  ($taille_libre_disque)/1GB
$taille_libre_disque_go = "{0:N2}" -f  $taille_libre_disque_go
Write-Output "L'espace libre du disque est de $taille_libre_disque Ko soit $taille_libre_disque_go Go ." >> $pathlog\ecombox.log


$taux_espace_libre_disque = $taille_libre_disque/$taille_disque
$taux_espace_libre_disque_pourcent="{0:p4}" -f ($taux_espace_libre_disque)
Write-Output "Le taux d'espace libre du disque est de $taux_espace_libre_disque_pourcent." >> $pathlog\ecombox.log

$espace_libre_disque_pourcent="{0:p2}" -f ($taux_espace_libre_disque)

if (($taux_espace_libre_disque -le 0.2) -or ($taille_libre_disque -le 40)) {

   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Optimisation de l'espace" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log
   Write-Output "Il reste $espace_libre_disque_pourcent de libre sur le disque ce qui représente $taille_libre_disque_go Go" >> $pathlog\ecombox.log
   Write-Output "" >> $pathlog\ecombox.log

   $reduc = popupExclamation -titre "Réduction de l'espace disque utilisé par les sites" -message "Votre espace libre est égal $espace_libre_disque_pourcent ce qui représente $taille_libre_disque_Go Go. `n`nLe système va tenter d'optimiser l'espace occupé par l'e-comBox avant de lancer l'application. `n`n Merci de ne pas redémarrer Docker. Le système procèdera au redémarrage à la fin des opérations." 
   
   $optimisation = optimisationEspace
}

}


Function lanceURL
{
      $config = valeurConfig

      # Récupération du l'adresse IP dans le fichier de paramètre      
      $ip_host = $config.ADRESSE_IP
      
      # Récupération du port (8888 par défaut) dans le fichier paramètre
      $ecb_port = $config.PORT_ECB

      # Construction de l'URL
      $url_acces_ecb = -join("$ip_host",":","$ecb_port")
      
      # reconnexion du registry au réseau de l'e-comBox (fix bug)
      docker network disconnect bridge_e-combox e-combox_registry
      docker network connect bridge e-combox_registry
      docker network connect bridge_e-combox e-combox_registry
      docker network disconnect bridge e-combox_registry

      # Détection du navigateur par défaut et lancement de l'URL
      New-PSDrive -Name HKCU -PSProvider Registry -Root HKEY_CURRENT_USER 2>> $null
      $Browser=Get-ItemProperty HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\.html\UserChoice | Select-Object -ExpandProperty ProgId
      Switch ($Browser) {
               'ChromeHTML' { Write-Output "Le navigateur par défaut est Google Chrome" >> $pathlog\ecombox.log
                              [System.Diagnostics.Process]::Start("chrome.exe", "--incognito $url_acces_ecb") }

               'FirefoxHTML-308046B0AF4A39CB' { Write-Output "Le navigateur par défaut est Mozilla FireFox" >> $pathlog\ecombox.log
                                                [System.Diagnostics.Process]::Start("firefox.exe","-private-window $url_acces_ecb")}

               'MSEdgeHTM' { Write-Output "Le navigateur par défaut est Microsoft Edge" >> $pathlog\ecombox.log
                            [system.Diagnostics.Process]::Start("msedge","-inPrivate $url_acces_ecb") }

                default { Write-Output "Le navigateur par défaut n'a pas pu être déterminé" >> $pathlog\ecombox.log
                          Start-Process "http://$url_acces_ecb/" *>> $pathlog\ecombox.log }
            }
      
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - L'application e-comBox a été lancée dans le navigateur." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      popupInformation -titre "Lancement d'e-combox" -message "L'application e-comBox a été lancée dans le navigateur."
}


Function recupInfosAPIportainer
{
  # Récupération du port (8880 par défaut) dans le fichier paramètre (à utiliser dans la version finale)
  $config = valeurConfig
  $portainer_port = $config.PORT_PORTAINER


  # Récupération du mot de passe de portainer (portnairAdmin par défaut) (à utiliser dans la version finale)
  $portainer_pass = recupMDPportainer
  $url_portainer_api = "http://localhost:$portainer_port/portainer/api/auth"

  Write-Host "Tentative de connexion à l'API de portainer..."
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "Tentative de connexion à l'API de portainer..." >> $pathlog\ecombox.log

  # Récupération du jeton
  # Utilisation de la commande curl.exe car il y a un alias sur curl vers invoke_webrequest et dont la commande ne fonctionne pas sur ps 5...
  $token_portainer = curl.exe -s -X POST --header "Content-Type: application/json" --insecure -d "{\""username\"":\""admin\"",\""password\"":\""$portainer_pass\""}" "$url_portainer_api"
  #write $token_portainer

  if ($token_portainer -match "jwt") {
     Write-Host " ... success de connexion à l'API de portainer."
     Write-Host ""
     Write-Output " ... success de connexion à l'API de portainer." >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log
     } 
     else {
          Write-Host "Impossible d'accéder à l'API de Portainer."
          Write-Host ""
          Write-Output "Impossible d'accéder à l'API de Portainer." >> $pathlog\ecombox.log
          Write-Output "" >> $pathlog\ecombox.log
     }

  $token_portainer=$token_portainer.Split("{""}")[4]
  #Write-Host "Le token est $token_portainer"

  # Récupération des stacks
  $url_portainer_stacks = "http://localhost:$portainer_port/portainer/api/stacks"
  $stacks = curl.exe -s -H "Authorization: Bearer $token_portainer" "$url_portainer_stacks"
  
  return $token_portainer,$url_portainer_stacks,$stacks
}


Function assoc_conteneurs_stacks {
$OutputEncoding = [Console]::OutputEncoding
  Write-Host "Récupération des conteneurs et association avec les stacks dans un fichier json..."
  Write-Host ""
  Write-Output "Récupération des conteneurs et association avec les stacks dans un fichier json..." >> $pathlog\ecombox.log
  Write-Output ""  >> $pathlog\ecombox.log

# Si un fichier existe on le remplace
  If (Test-Path "$path_migration\conteneurs_stacks.json") {
     $newName = "conteneurs_stacks_$(Get-date -format 'ddMMyy_HHmmss').json"
     Rename-Item -Path "$path_migration\conteneurs_stacks.json" -NewName $newName
  }

  # Récupération des conteneurs
  # Lancement de tous les conteneurs
  Write-Host "Démarrage des conteneurs..."
  Write-Output "`nDémarrage des conteneurs..." >> $path_migration\migration.log
  docker start $(docker ps -aq) *>> $path_migration\migration.log

  $conteneurs = (docker inspect --format='{{.Name}}' $(docker ps -aq --no-trunc) | Select-String -Pattern '-db-|portainer-|nginx|-combox|docker-' -NotMatch).Line.TrimStart("/")
  
  # Création du fichier au bon format
  "[" > $path_migration\conteneurs_stacks.tmp.json

  # Pour chaque nom de conteneur, suppression des "-" pour avoir le nom du stack associé et écriture de chaque ligne du fichier json
  foreach($conteneur in $conteneurs) {    
      $stack_ass = $conteneur.Replace("-","")
      @{"stack"="$stack_ass";"conteneur"="$conteneur";} | ConvertTo-Json >> $path_migration\conteneurs_stacks.tmp.json
      "," >> $path_migration\conteneurs_stacks.tmp.json
      }

      Get-Content $path_migration\conteneurs_stacks.tmp.json | Select -SkipLast 1 | Set-Content $path_migration\conteneurs_stacks.json -Force
      "]" >> $path_migration\conteneurs_stacks.json

      Set-Content $path_migration\conteneurs_stacks.json -Encoding Ascii -Value (Get-Content $path_migration\conteneurs_stacks.json).Replace("`0",'')
      
      Write-Output "`nle contenu du fichier conteneurs_stacks est :`n" >> $path_migration\migration.log
      (Get-Content -Raw $path_migration\conteneurs_stacks.json) | ConvertFrom-Json *>> $path_migration\migration.log

}


Function stopStacks {
$pathlog="$env:USERPROFILE\.docker\logEcombox"

$token_portainer,$url_portainer_stacks,$stacks = recupInfosAPIportainer
Write-Output "Dans la fonction stop :" >> $pathlog\ecombox.log
Write-Output "Le token est $token_portainer" >> $pathlog\ecombox.log
Write-Output "L'URL api stacks est $url_portainer_stacks" >> $pathlog\ecombox.log
Write-Output "Les stacks sont $stacks" >> $pathlog\ecombox.log


#($stacks | ConvertFrom-Json) | select Id, Name
#($stacks | ConvertFrom-Json)[1]
#($stacks | ConvertFrom-Json)[1].Id

$stacks = ($stacks | ConvertFrom-Json)

foreach($stack in $stacks)
    {
      $id = $stack.Id
      $name = $stack.Name
      $status = $stack.Status

      # Arrêt du stack si celui-ci n'est pas déjà arrêté
      
      if ($status -eq "1")
      {     
         Write-Host "Arrêt du stack $id : $name ..."
         Write-Output "Arrêt du stack $id : $name ..." >> $pathlog\ecombox.log

         $stop = curl.exe -s "$url_portainer_stacks/$id/stop" -X POST -H "Authorization: Bearer $token_portainer"
         #Write-Host "Retour de l'arrêt : $stop"
         Write-Output "Retour de l'arrêt : $stop" >> $pathlog\ecombox.log
      }
      
    }
}


Function updateStacks { 
  $OutputEncoding = [Console]::OutputEncoding

  # Gestion des logs de migration
  
  Write-Output ""
  Write-Output "Création d'un fichier de log spécifique pour la migration" >> $pathlog\ecombox.log
  Write-Output ""

  If (Test-Path "$path_migration\migration.log") {
     $newName = "migration_$(Get-date -format 'ddMMyy_HHmmss').log"
     Rename-Item -Path "$path_migration\migration.log" -NewName $newName
  }
  New-Item -Path "$path_migration\migration.log" -ItemType file -force

  $token_portainer,$url_portainer_stacks,$stacks = recupInfosAPIportainer
  assoc_conteneurs_stacks
  $p_prune = 'false'

  $config = valeurConfig
  $ecb_docker_host = $config.ADRESSE_IP
  $nginx_port = $config.PORT_RP

  Write-Output "Dans la fonction update :" >> $path_migration\migration.log
  Write-Output "Le token est $token_portainer" >> $path_migration\migration.log
  Write-Output "L'URL api stacks est $url_portainer_stacks" >> $path_migration\migration.log
  Write-Output "Les stacks sont $stacks" >> $path_migration\migration.log

  # Récupération des docker-compose

  if (Test-Path "$path_migration\e-comBox_docker-compose")
  {
     Remove-Item -Path $path_migration\e-comBox_docker-compose -Recurse -Force
  }

  Set-Location $path_migration *>> $path_migration\migration.log
  git clone -b $BRANCHE https://gitlab.com/e-combox/e-comBox_docker-compose.git *>> $path_migration\migration.log


  # Sauvegarde du fichier stacks.json
  If (Test-Path "$path_migration\stacks.json") {
       $newName = "stacks_$(Get-date -format 'ddMMyy_HHmmss').json"
       Rename-Item -Path "$path_migration\stacks.json" -NewName $newName
  }
  $stacks >> $path_migration\stacks.json
  $stacks = ($stacks | ConvertFrom-Json)


  foreach($stack in $stacks)
    {
      $id = $stack.Id
      $name = $stack.Name
      $dc_yml = $stack.EntryPoint
      $env_json = $stack.Env | ConvertTo-Json
       
      if (!$env_json) {
    
         #Write-Host "Ajout des variables d'environnement au stack $name qui a pour ID $id."
         Write-Output "`nAjout des variables d'environnement au stack $name qui a pour ID $id." >> $path_migration\migration.log         
         $file_json = (Get-Content -Raw $path_migration\conteneurs_stacks.json).Replace("`0",'') | ConvertFrom-Json     
        
         $nom_conteneur = ($file_json | Where {$_.stack -eq "$name"}).conteneur  
         
         #Write-Host "Traitement du conteneur : $nom_conteneur"
         Write-Output "Traitement du conteneur : $nom_conteneur" >> $path_migration\migration.log
         
         # Détermination du suffixe        
         $carsupr = ($nom_conteneur).Split("-")[0]   
         $suffixe = ($nom_conteneur).TrimStart("$carsupr").TrimStart("-")
         
         Write-Output "carsupr est $carsupr" >> $path_migration\migration.log
         Write-Output "Le suffixe est $suffixe" >> $path_migration\migration.log
                  
         # Détermination du mot de passe de la base de données         `
         $file_json_env = (docker inspect --format='{{json .Config.Env}}' $nom_conteneur | convertFrom-Json)
         $dbpass = ($file_json_env | Select-String -Pattern "DB_PASS=").ToString().Split("=")[1]
         Write-Output "Le mot de passe est $dbpass" >> $path_migration\migration.log

         # Affectation de la variable $env_json initialement vide
 
         $env_suffixe_json = @{"value"="$suffixe";"name"="SUFFIXE";}
         $env_dbpass_json = @{"value"="$dbpass";"name"="DB_PASS";}
         $env_ecbdockerhost_json = @{"value"="$ecb_docker_host";"name"="ECB_DOCKER_HOST";}
         $env_nginxport_json = @{"value"="$nginx_port";"name"="NGINX_PORT";}                 
         
         $env_array = New-Object System.Collections.ArrayList
         $env_array.Add($env_suffixe_json)         
         $env_array.Add($env_dbpass_json)
         $env_array.Add($env_ecbdockerhost_json)         
         $env_array.Add($env_nginxport_json)
         
         $env_json = @{"Env"=$env_array} | ConvertTo-Json -Compress
         $env_Json =$env_json.TrimStart("{""Env"":").TrimEnd("}")
      }                              
          
      # Mise à jour du stack

      Write-Host "Traitement du stack $name qui a pour ID $id."
      Write-Output "`nTraitement du stack $name qui a pour ID $id." >> $path_migration\migration.log

      Write-Output "la variable d'environnement est $env_json" *>> $path_migration\migration.log

      $target_yml = "$path_migration\e-comBox_docker-compose\$dc_ymL"
      
      Write-Output "Le fichier yml cible est $target_yml" *>> $path_migration\migration.log               
     
      $dcompose = (Get-Content $target_yml -Raw).Replace("`r",'').Replace('"','\"').Replace("`n","\n")           
      #Write-Host $dcompose
          
      Write-Host "/-----Lecture du fichier YML--------"
      #Write-Host "$dcompose"
      #Write-Host "\---------------------"
      
      $data_prefix = -join('{"Id":"',"$id",'",','"StackFileContent":"')
      $data_suffix = -join('","Env":',"$env_json",',"Prune":',"$p_prune",'}')
      
      Write-Host "/~~~~Traitement du fichier YML - Conversion du JSON~~~~~~"
      Write-Host "$data_prefix$dcompose$data_suffix"
      Write-Host "\~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

      Write-Output "/~~~~Traitement du fichier UML - Conversion du JSON~~~~~~" *>> $path_migration\migration.log
      Write-Output "$data_prefix$dcompose$data_suffix" *>> $path_migration\migration.log
      Write-Output "\~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" *>> $path_migration\migration.log
            
      $json_tmp = -join("$data_prefix","$dcompose","$data_suffix")
      
      #$json_tmp = $json_tmp.Replace("`r`n","\n")
      Write-Output $json_tmp > json.tmp
      
      # Réencoder en ASCII sinon il n'est pas possible de lire le fichier vi l'appel de l'API
      Set-Content json.tmp -Encoding Ascii -Value (Get-Content json.tmp)

      Write-Host "Mise à jour du stack $nom_stack..."
      Write-Output "Mise à jour du stack $nom_stack..." *>> $path_migration\migration.log

      $url_update = -join("$url_portainer_stacks","/","$id","?endpointId=1")

      $update = curl.exe -s "$url_update" -X PUT -H "Authorization: Bearer $token_portainer" -H "Content-Type: application/json;charset=UTF-8" -H 'Cache-Control: no-cache' --data-binary "@json.tmp"
      
      # Write-Host "Retour de la mise à jour : $UPDATE"
      Write-Output "Retour de la mise à jour : $UPDATE" *>> $path_migration\migration.log
      
      stopStacks
    }
}


function Select-Folder($message='Selectionner un répertoire de sauvegarde', $path = 0)
{
   $object = New-Object -comObject Shell.Application
 
   $folder = $object.BrowseForFolder(0, $message, 0, $path)
   if ($folder -ne $null)
   {
      $folder.self.Path
   }
}


Function sauvVolumes 
{

   Write-Host "`nArrêt des sites encore actifs..."
   Write-Output "`nArrêt des sites encore actifs..." >> $pathlog\ecombox.log

   stopStacks

   # Nom du dossier existant
   $folderpath=Select-Folder "Selectionner le dossier dans lequel vous voulez sauvegarder vos sites."

   # Choix du dossier de sauvegarde
   $date = Get-date -format 'ddMMyy'
   $nom_dossier = -join ("sitesV3_","$date")

   $reponse = popupQuestion -titre "Nom du dossier de sauvegarde" -message "Le nom du dossier de sauvegarde proposé est $nom_dossier. `n`nC'est ce dernier qui contiendra tous les éléments essentiels à une éventuelle restauration. Cela vous convient-il ? `n`nCliquez sur NON pour choisir un autre nom de dossier." -Foreground Yellow 
   if ($reponse -ne 6) 
      {
         $nom_dossier = Read-Host "Saisissez un autre nom de dossier de sauvegarde."          
      } 
      
   $dest = -join ("$folderpath","\","$nom_dossier")
   #Write-Host " rep dest est $dest"
   
   Write-Host "`nLa sauvegarde des sites est en cours, merci de patienter, cela peut prendre du temps..."  

   #$path_docker='\\wsl$\docker-desktop-data\version-pack-data\community\docker'
   $source = -join ("$path_docker","\","volumes")
      
   robocopy $source $dest * /mt /mir /xj

   $fin = popupExclamation -titre "Sauvegarde terminée" -message "La sauvegarde est terminée." 
           
}


Function restaureVolumes
{
   # Choix du dossier à partir duquel il faut restaurer
   $path_restaure=Select-Folder "Selectionner le dossier à partir duquel vous voulez restaurer vos sites."
   
   $source = $path_restaure
   $dest = -join ("$path_docker","\","volumes") 

   $confirme = popupStop -titre "Restauration des sites" -message "Vous vous apprêtez à remplacer vos sites existants. `n`nVeuillez confirmer !"
        if ($confirme -eq 6) 
           {              
              # Lancement de la restauration
              Write-Host "`nArrêt des sites encore actifs..."
              Write-Output "`nArrêt des sites encore actifs..." >> $pathlog\ecombox.log
              stopStacks

              # Arrêt de tous les services
              Write-Host "Arrêt des services…"
 
              # Reverse proxy
              Set-Location -Path $path_rv
              docker-compose down
 
              # Registry
              docker stop e-combox_registry
  
              # Serveur git
              docker stop e-combox_gitserver
  
              # Portainer
              Set-Location -Path $path_portainer
              docker-compose down
  
              # E-combox
              docker stop e-combox

              Write-Host "`nLa restauration des sites est en cours, merci de patienter, cela peut prendre du temps..."
              robocopy $source $dest * /mt /mir /xj

              # Démarrage de tous les services
              Write-Host "Démarrage des services…"
 
              # Reverse proxy
              Set-Location -Path $path_rv
              docker-compose up -d
 
              # Registry
              docker start e-combox_registry
  
              # Serveur git
              docker start e-combox_gitserver
  
              # Portainer
              Set-Location -Path $path_portainer
              docker-compose up -d
  
              # E-combox
              docker start e-combox

              Write-Output "" >> $pathlog\ecombox.log
              Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Restauration terminée" >> $pathlog\ecombox.log
              
              $fin = popupExclamation -titre "restauration terminée" -message "La restauration est terminée. `nVous pouvez relancer l'e-comBox après avoir vidé le cache de votre navigateur. `n`nSi l'environnement n'était pas le même à la date du point de restauration, il est nécessaire de procéder à une reconfiguration de l'environnement." 
           }  
              else {
                 Write-Output "L'utilisateur a renoncé à la restauration."  >> $pathlog\ecombox.log
                 Write-Output ""  >> $pathlog\ecombox.log
               } 

}