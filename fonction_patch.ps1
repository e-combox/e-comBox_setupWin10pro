﻿Function patchSetup {
  
    if (Test-Path "$pathscripts\installCorrectif.ps1") {

       # Vérification de la politique courante d'exécution des scripts powershell
       $policy=Get-ExecutionPolicy -Scope CurrentUser
       Write-Output "`nLa politique d'exécution des scripts PS est $policy" >> $pathlog\ecombox.log

       # Désactivation de la politique si elle n'est pas déjà désactivée
       if ($policy -ne "Unrestricted") { Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted }
    
       $newPolicy=Get-ExecutionPolicy -Scope CurrentUser 
       Write-Output "`nL'éventuelle nouvelle politique d'exécution des scripts PS est $newPolicy" >> $pathlog\ecombox.log

       # Installation du correctif
       Set-Location -Path $pathscripts

       $job = Start-Job -FilePath 'installCorrectif.ps1' 
       $job | Wait-Job | Receive-Job
  
       Write-Output "`nLe correctif $version_correctif_gitlab a été appliqué" >> $pathlog\ecombox.log

       # Réactivation de la politique existante
       if ($policy -ne "Unrestricted") { Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy $policy } 
    
       $newPolicy=Get-ExecutionPolicy -Scope CurrentUser 
       Write-Output "`nL'éventuelle nouvelle politique d'exécution des scripts PS est $newPolicy" >> $pathlog\ecombox.log
       
       Set-Location -Path $env:USERPROFILE

    } else { Write-Output "`nLe fichier d'installation du correctif n'existe pas" >> $pathlog\ecombox.log}
    
}
